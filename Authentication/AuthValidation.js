var http = require('http');
var Q = require('q');
var Auth = require('../Authentication/AuthenticationSchema.js');
var token = require('../Authentication/AccessToken.js');
var moduleConfig = require('../config.js');

module.exports = {
    validateToken: function(userToken) {
        var def = Q.defer();
        var accessToken = token.getToken().access_token;
        Auth.find({ "token": userToken }, function(err, user) {
            if (user.length) {
                def.resolve(user);
            } else {
                var options = {
                    host: 'api.qa1.nbos.in',
                    method: 'GET',
                    headers: {
                        'Authorization': 'Bearer ' + accessToken,
                        'X-N-ModuleKey': moduleConfig.moduleKey
                    }
                };
                options.path = "/api/oauth/v0/tokens/" + userToken;
                var response = "";
                var req = http.request(options, function(res) {
                    res.setEncoding('utf8');
                    if (res.statusCode == 404) {
                        def.reject("User not found");
                    } else {
                        res.on('data', function(chunk) {
                            response += chunk;
                        });

                        res.on('end', function() {
                            var json = JSON.parse(response);
                            var user = new Auth();
                            user.username = json.username;
                            user.token = json.token;
                            user.clientId = json.clientId;
                            user.member = json.member.uuid;
                            user.expriation = json.expriation;
                            user.tenantId = json.tenantId;
                            user.save(function(err, user_saved) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    console.log("Saved");
                                    def.resolve(json);
                                }
                            });
                        });
                    }
                }, function(res) {
                    def.reject("error")
                });

                req.write("hello");
                req.end();
            };
        });

        return def.promise;
    }
};
