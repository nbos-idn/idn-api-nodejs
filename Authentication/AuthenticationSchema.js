var mongoose = require('mongoose');

var AuthSchema = new mongoose.Schema({
    username: String,
    clientId: String,
    member: String,
    token: String,
    tenantId: String,
    expiration: { type: Date},

});

module.exports = mongoose.model('Auth', AuthSchema);
